const express = require("express");
const router = express.Router();
const { PersonController } = require("../controllers");
const { AuthMiddleware } = require("../middlewares");

router.get("/personas", PersonController.list);
router.post("/persona", PersonController.create);
router.put("/persona/:id", PersonController.update);
router.delete("/persona/:id", PersonController.remove);

module.exports = router;
