# PRACTICA - NODEJS - MODULO I

### Install dependencies

```
yarn install
```

### Routes

```
localhost:3000/personas     // GET
localhost:3000/persona      // POST
localhost:3000/persona/:id  // UPDATE
localhost:3000/persona/:id  // DELETE
```

### Data Base - MongoDB

```
docker pull mongo

docker run --name mymongo -d mongo
```

### Setting DB

change IP Mongo Host and copy files

```
cp constants.sample.js constants.js

cp db.sample.js db.js
```
