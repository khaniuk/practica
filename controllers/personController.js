const { PersonModel } = require("../models");

const list = async (req, res) => {
  try {
    const response = await PersonModel.find();
    res.status(200).send(response);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

const create = async (req, res) => {
  try {
    const response = await PersonModel.create(req.body);

    res.status(201).send(response);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

const update = async (req, res) => {
  try {
    await PersonModel.findByIdAndUpdate(
      req.params.id,
      {
        nombres: req.body.nombres || PersonModel.nombres,
        paterno: req.body.paterno || PersonModel.paterno,
        materno: req.body.materno || PersonModel.materno,
        dni: req.body.dni || PersonModel.dni,
      },
      { new: true }
    );
    res.status(200).send({
      message: "Updated",
    });
  } catch (err) {
    res.status(500).send({
      message: "Error update " + err.message,
    });
  }
};

const remove = async (req, res) => {
  try {
    await PersonModel.findByIdAndRemove(req.params.id);
    res.send({ message: "Deleted" });
  } catch (err) {
    res.status(500).send({
      message: "Not delete: " + err.message,
    });
  }
};

module.exports = {
  list,
  create,
  update,
  remove,
};
