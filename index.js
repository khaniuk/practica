const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require("body-parser");
const { PersonRoute } = require("./routes");

// DATABASE
const mongoose = require("mongoose");
const { db } = require("./config");
mongoose.connect(db.urlConn, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(bodyParser.json());

app.use(PersonRoute);

app.listen(port, () => {
  console.log(`Start on port ${port}`);
});
