const mongoose = require("mongoose");
const { Schema } = mongoose;

const PersonSchema = new Schema({
  nombres: String,
  paterno: String,
  materno: String,
  dni: String,
});

const Person = mongoose.model("person", PersonSchema);

module.exports = Person;
